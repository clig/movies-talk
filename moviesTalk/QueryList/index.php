<?php
// 连接数据库、设置字符集
include '../config.php';
// 创建预处理语句
$stmt=mysqli_stmt_init($link);
//编写预处理查询sql语句
$sql = "select name,download from movie";
if (mysqli_stmt_prepare($stmt,$sql))
{    
    // 执行查询
    mysqli_stmt_execute($stmt);
    // 从准备好的语句获取结果集
    $query = mysqli_stmt_get_result($stmt);
    echo "<div style='background-color: #EAEAEF;text-align:center;'>";
    while ($result = mysqli_fetch_assoc($query)) {
        echo "<h3 style='margin:5px auto'><a style='color: #000;text-decoration: none;' target='_blank' href=".$result['download'].">".$result['name']."</a></h3>";
    }
    echo "</div>";
    // 关闭预处理语句
    mysqli_stmt_close($stmt);
}
//关闭连接
mysqli_close($link);
?>