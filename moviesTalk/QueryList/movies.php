<?php
// 引入自动加载
include 'vendor/autoload.php';
// 导入QueryList
use \QL\QueryList;
// 书写
$url = "https://www.ygdy8.net/html/gndy/dyzz/";
// 获取规则
$reg=array(
	// 电影名
	"name"=>array(".co_content8 .tbspan a","text"),
	// 电影路径
	"url"=>array(".co_content8 .tbspan a","href")
    );
// 连接数据库
$pdo = new PDO("mysql:host=127.0.0.1;dbname=talk","root","root");
// 清空数据表
$pdo->exec("truncate movie");
// 准备sql语句
$sql = "insert into movie(name,download) values(?,?)";
$stmt = $pdo->prepare($sql);
// 发送请求，单线程爬虫
// $data = QueryList::Query($url,$reg,'','UTF-8','GBK')->getData(function($item){
// 	// 获取的地址
// 	$newUrl = "https://www.ygdy8.net".$item['url'];
// 	// 新闻内容的获取规则
// 	$newReg = array(
// 		"download" => array(".co_content8 #Zoom a","href"),
// 		);
// 	// 发送请求
// 	$newData = QueryList::Query($newUrl,$newReg,'','UTF-8','GBK')->data;
// 	foreach ($newData as $download) {
// 		if(strpos($download['download'],'magnet:?xt=urn:btih:') === 0){ 
// 			// 数据入库
// 			$GLOBALS['stmt']->execute([$item['name'],$download['download']]);
// 		}
// 	}
// });
// exit();
// 发送请求，多线程爬虫
$data = QueryList::Query($url,$reg,'','UTF-8','GBK')->data;
foreach ($data as $array) {
	$urlList[] = "https://www.ygdy8.net".$array['url'];
}
//多线程扩展
QueryList::run('Multi',[
    //待采集链接集合
    'list' => $urlList,
    'curl' => [
        'opt' => array(
            //这里根据自身需求设置curl参数
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_AUTOREFERER => true,
            //........
                ),
        //设置线程数
        'maxThread' => 100,
        //设置最大尝试数
        'maxTry' => 3,
    ],
    'success' => function($a){
        //采集规则
        $reg=array(
			"name"=>array(".bd3 .bd3l .co_area2 .title_all h1>font","text"),
			"download" => array(".co_content8 #Zoom a","href"),
		);
        $GLOBALS['arr'][] = QueryList::Query($a['content'],$reg)->data;
    }
]);
for($j=0; $j < count($arr); $j++) {
	$downloadData[$j]['name'] = $arr[$j][0]['name'];
	foreach($arr[$j] as $value){
		if(strpos($value['download'],'magnet:?xt=urn:btih:') === 0){
			$downloadData[$j]['download'] = $value['download']; 
		}
	}
}
foreach($data as $value1){
	foreach($downloadData as $value2){
		if($value1['name'] == $value2['name']){
			// 数据入库
			$GLOBALS['stmt']->execute([$value1['name'],$value2['download']]);
		}
	}
}